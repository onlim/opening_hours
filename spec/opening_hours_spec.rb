# frozen_string_literal: true

RSpec.describe OpeningHours do
  it "has a version number" do
    expect(OpeningHours::VERSION).not_to be nil
  end

  describe "#list" do
    after do
      Timecop.return
      I18n.locale = I18n.default_locale
    end

    before do
      I18n.locale = "en"

      Timecop.freeze(Time.parse("2019-01-21 18:53:35 +0200")) # a Monday
    end

    context "when no opening hours definded" do
      it "returns proper text" do
        response = described_class.list({})

        expect(response.first).to eq(I18n.t("no_opening_hours_set"))
      end
    end

    context "when opening hours without period (always)" do
      let!(:entity) do
        {
          "openingHoursSpecification": [
            {
              "name" => "Whatever",
              "@type": ["OpeningHoursSpecification"],
              "allDay": false,
              "opens" => "17:00:00",
              "closes" => "22:00:00",
              "dayOfWeek": ["Monday"]
            },
            {
              "name" => "Whatever",
              "@type": ["OpeningHoursSpecification"],
              "allDay": true,
              "dayOfWeek": ["Tuesday"]
            },
            {
              "name" => "Whatever",
              "@type": ["OpeningHoursSpecification"],
              "allDay": false,
              "opens" => "21:00:00",
              "closes" => "22:00:00",
              "dayOfWeek": ["Wednesday"]
            }
          ],
          "opening_hours_description_translations" => {
            "en" => "Winter is coming!",
            "de" => "Der Winter kommt!"
          }
        }
      end

      it "returns correct opening hours without period" do
        response = described_class.list(entity)

        expect(response.first).not_to include("Valid from 01.01.2019 to 01.03.2019")
        expect(response.first).to include("Monday: 17:00 - 22:00")
        expect(response.first).to include("Tuesday: 24 hours opened")
        expect(response.first).to include("Wednesday: 21:00 - 22:00")
        expect(response.first).to include("Thursday: Closed")
      end
    end

    context "when no opening hours but opening hours description" do
      let!(:entity) do
        {
          "openingHoursSpecificationSet" => [],
          "opening_hours_description_translations" => {
            "en" => "Winter is coming!",
            "de" => "Der Winter kommt!"
          }
        }
      end

      it "returns opening hours description" do
        response = described_class.list(entity)

        expect(response.first).to include("Winter is coming")
      end
    end

    context "when period is provided (from..to)" do
      let!(:entity) do
        {
          "openingHoursSpecification": [
            {
              "name" => "Whatever",
              "@type": ["OpeningHoursSpecification"],
              "allDay": false,
              "opens" => "17:00:00",
              "closes" => "22:00:00",
              "dayOfWeek": ["Monday"],
              "validFrom" => "2019-01-01T00:00:00.000+00:00",
              "validThrough" => "2019-03-01T00:00:00.000+00:00"
            },
            {
              "name" => "Whatever",
              "@type": ["OpeningHoursSpecification"],
              "allDay": true,
              "dayOfWeek": ["Tuesday"],
              "validFrom" => "2019-01-01T00:00:00.000+00:00",
              "validThrough" => "2019-03-01T00:00:00.000+00:00"
            },
            {
              "name" => "Whatever",
              "@type": ["OpeningHoursSpecification"],
              "allDay": false,
              "opens" => "21:00:00",
              "closes" => "22:00:00",
              "dayOfWeek": ["Wednesday"],
              "validFrom" => "2019-01-01T00:00:00.000+00:00",
              "validThrough" => "2019-03-01T00:00:00.000+00:00"
            }
          ],
          "opening_hours_description_translations" => {
            "en" => "Winter is coming!",
            "de" => "Der Winter kommt!"
          }
        }
      end

      it "returns an array" do
        response = described_class.list(entity)

        expect(response).to be_a(Array)
      end

      it "returns the opening hours text" do
        response = described_class.list(entity)

        expect(response.first).not_to be_empty
        expect(response.first).to be_a(String)
      end

      it "returns correct opening hours" do
        response = described_class.list(entity)

        expect(response.first).to include("Valid from 01.01.2019 to 01.03.2019")
        expect(response.first).to include("Monday: 17:00 - 22:00")
        expect(response.first).to include("Tuesday: 24 hours opened")
        expect(response.first).to include("Wednesday: 21:00 - 22:00")
        expect(response.first).to include("Thursday: Closed")

        expect(response[1]).to include("Winter is coming")
      end
    end

    context "when current date not included in any sets" do
      let!(:entity) do
        {
          "openingHoursSpecification": [
            {
              "name" => "Whatever",
              "@type": ["OpeningHoursSpecification"],
              "allDay": false,
              "opens" => "17:00:00",
              "closes" => "22:00:00",
              "dayOfWeek": ["Monday"],
              "validFrom" => "2019-01-01T00:00:00.000+00:00",
              "validThrough" => "2019-01-20T00:00:00.000+00:00"
            },
            {
              "name" => "Whatever 2",
              "@type": ["OpeningHoursSpecification"],
              "opens" => "17:00:00",
              "closes" => "22:00:00",
              "allDay": false,
              "dayOfWeek": ["Tuesday"],
              "validFrom" => "2019-04-01T00:00:00.000+00:00",
              "validThrough" => "2019-06-30T00:00:00.000+00:00"
            }
          ]
        }
      end

      it "returns the opening hours text" do
        response = described_class.list(entity)

        expect(response.first).to eq(I18n.t("no_opening_hours_set"))
      end
    end

    context "when current date included in set" do
      let!(:entity) do
        {
          "openingHoursSpecification": [
            {
              "name" => "Whatever",
              "@type": ["OpeningHoursSpecification"],
              "allDay": false,
              "opens" => "17:00:00",
              "closes" => "22:00:00",
              "dayOfWeek": ["Monday"],
              "validFrom" => "2019-01-01T00:00:00.000+00:00",
              "validThrough" => "2019-03-01T00:00:00.000+00:00"
            },
            {
              "name" => "Whatever 2",
              "@type": ["OpeningHoursSpecification"],
              "opens" => "17:00:00",
              "closes" => "22:00:00",
              "allDay": false,
              "dayOfWeek": ["Tuesday"],
              "validFrom" => "2019-04-01T00:00:00.000+00:00",
              "validThrough" => "2019-06-30T00:00:00.000+00:00"
            }
          ]
        }
      end

      it "selects the right opening hours set" do
        response = described_class.list(entity)

        expect(response.first).to include("Valid from 01.01.2019 to 01.03.2019")
      end
    end
  end

  describe "#status" do
    let(:entity) do
      {
        "openingHoursSpecification": [
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "09:00:00",
            "closes" => "12:00:00",
            "dayOfWeek": ["Monday"]
          },
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "09:00:00",
            "closes" => "12:00:00",
            "dayOfWeek": ["Wednesday"]
          },
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "14:00:00",
            "closes" => "17:00:00",
            "dayOfWeek": ["Wednesday"]
          },
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "09:00:00",
            "closes" => "12:00:00",
            "dayOfWeek": ["Thursday"]
          },
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": true,
            "dayOfWeek": ["Friday"]
          },
        ]
      }
    end

    describe "when closed" do
      context "when before program starts" do
        it "returns proper text" do
          Timecop.freeze(2018, 1, 17, 8, 30, 0) # Wednesday

          response = described_class.status(entity)

          expect(response).to eq("Closed. Opens 09:00 Wednesday")

          Timecop.return
        end
      end

      context "when lunch break" do
        it "returns proper text" do
          Timecop.freeze(2018, 1, 17, 12, 30, 0) # Wednesday

          response = described_class.status(entity)

          expect(response).to eq("Closed. Opens 14:00 Wednesday")

          Timecop.return
        end
      end

      context "when after program" do
        it "returns proper text" do
          Timecop.freeze(2018, 1, 17, 18, 30, 0) # Wednesday

          response = described_class.status(entity)

          expect(response).to eq("Closed. Opens 09:00 Thursday")

          Timecop.return
        end
      end

      context "when closed all day" do
        it "returns proper text" do
          Timecop.freeze(2018, 1, 16, 11, 30, 0) # Tuesday

          response = described_class.status(entity)

          # If there is no current_spec we cannot determin next_index
          expect(response).to eq("Closed.")
          Timecop.return
        end
      end

      context "when open all day" do
        it "returns proper text" do
          Timecop.freeze(2018, 1, 19, 11, 30, 0) # Friday

          response = described_class.status(entity)

          expect(response).to eq("Open.")
          Timecop.return
        end
      end
    end

    describe "when open" do
      it "returns proper text" do
        Timecop.freeze(2018, 1, 17, 15, 30, 0) # Wednesday

        response = described_class.status(entity)

        expect(response).to eq("Open. Closes 17:00")

        Timecop.return
      end

      context "when less than an hour before close" do
        it "returns proper text" do
          Timecop.freeze(2018, 1, 17, 16, 30, 0) # Wednesday

          response = described_class.status(entity)

          expect(response).to eq("Open. Close soon: 17:00")

          Timecop.return
        end
      end

      context "when no opening hours found for the period" do
        let(:entity) do
          {
            "openingHoursSpecification": [
              {
                "name" => "Whatever",
                "@type": ["OpeningHoursSpecification"],
                "allDay": false,
                "opens" => "09:00:00",
                "closes" => "12:00:00",
                "dayOfWeek": ["Monday"],
                "validFrom" => "2019-01-01T00:00:00.000+00:00",
                "validThrough" => "2019-03-01T00:00:00.000+00:00"
              }
            ]
          }
        end

        it "returns proper text" do
          Timecop.freeze(2020, 1, 17, 9, 30, 0)

          response = described_class.status(entity)

          expect(response).to eq(I18n.t("no_opening_available"))

          Timecop.return
        end
      end
    end
  end

  describe "#today" do
    let(:entity) do
      {
        "openingHoursSpecification": [
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "09:00:00",
            "closes" => "12:00:00",
            "dayOfWeek": ["Monday"]
          },
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "09:00:00",
            "closes" => "12:00:00",
            "dayOfWeek": ["Wednesday"]
          },
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "14:00:00",
            "closes" => "17:00:00",
            "dayOfWeek": ["Wednesday"]
          },
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "09:00:00",
            "closes" => "12:00:00",
            "dayOfWeek": ["Thursday"]
          },
          {
            "name" => "Whatever",
            "@type": ["OpeningHoursSpecification"],
            "allDay": false,
            "opens" => "09:00:00",
            "closes" => "12:00:00",
            "dayOfWeek": ["Friday"]
          },
        ]
      }
    end

    it "returns proper working hours" do
      Timecop.freeze(2018, 1, 15, 16, 30, 0) # Monday

      response = described_class.today(entity)

      expect(response).to eq("09:00 - 12:00")

      Timecop.return
    end

    context "when multiple hours" do
      it "returns proper working hours" do
        Timecop.freeze(2018, 1, 17, 16, 30, 0) # Wednesday

        response = described_class.today(entity)

        expect(response).to eq("09:00 - 12:00, 14:00 - 17:00")

        Timecop.return
      end
    end

    context "when no opening hours defined" do
      it "returns proper text" do
        Timecop.freeze(2018, 1, 16, 16, 30, 0) # Thuesday

        response = described_class.today(entity)

        expect(response).to eq(I18n.t("no_opening_available"))

        Timecop.return
      end
    end

    context "when no opening hours set defined" do
      it "returns proper text" do
        Timecop.freeze(2018, 1, 16, 16, 30, 0) # Thuesday

        response = described_class.today({})

        expect(response).to eq(I18n.t("no_opening_available"))

        Timecop.return
      end
    end
  end
end
