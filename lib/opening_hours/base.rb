# frozen_string_literal: true

require "opening_hours/transform/schema_org"

module OpeningHours
  class Base
    WEEK_DAYS = %w[
      Monday
      Tuesday
      Wednesday
      Thursday
      Friday
      Saturday
      Sunday
    ].freeze

    def initialize(entity)
      @entity = transform(entity.with_indifferent_access)
      @time   = Time.zone.now.to_i
    end
    attr_reader :entity, :time

    private

    def transform(entity)
      if entity.key?("openingHoursSpecificationSet")
        Transform::SchemaOrg.new(entity).call
      else
        entity
      end
    end

    def select_opening_hours_set(specs)
      return [] if specs.blank?
      return specs if always?(specs)

      specs.select do |opening_hours_set|
        start_date = Date.parse(opening_hours_set["validFrom"] || Date.today.to_s)
        end_date   = Date.parse(opening_hours_set["validThrough"] || Date.today.to_s)
        range      = (start_date..end_date)

        range.cover?(Date.today)
      end
    end

    def always?(specs)
      specs.any? do |set|
        set["validFrom"].blank? && set["validThrough"].blank?
      end
    end

    def working_hours(opening_hours)
      hours  = []
      ranges = hours_range(opening_hours)

      ranges.compact.sort_by(&:first).each do |range|
        hours << "#{range.first.strftime('%H:%M')} - #{range.last.strftime('%H:%M')}"
      end

      hours = hours.join(", ")
    end

    def hours_range(sets)
      date = Date.today

      sets.map do |day|
        opens  = Time.parse("#{date} #{day['opens']}")
        closes = Time.parse("#{date} #{day['closes']}")

        opens..closes
      end
    end
  end
end
