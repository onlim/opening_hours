# frozen_string_literal: true

module OpeningHours
  class List < Base
    def call
      answer_from_sets        = from_sets(entity).presence
      answer_from_description = from_description(entity).presence

      if answer_from_sets.blank? && answer_from_description.blank?
        return [I18n.t("no_opening_hours_set")]
      end

      [answer_from_sets, answer_from_description].compact
    end

    private

    def from_description(entity)
      text = entity.dig("opening_hours_description_translations", I18n.locale)
      return text if text.present?

      opening_description = entity["openingHoursSpecificationDescription"]
      return if opening_description.blank?

      hash = opening_description.find { |obj| obj["@language"] == I18n.locale.to_s }
      return if hash.blank?

      hash["@value"]
    end

    def from_sets(entity)
      opening_hours = select_opening_hours_set(entity["openingHoursSpecification"])
      return [] if opening_hours.blank?

      Logging.logger.info("Using openingHours: #{opening_hours}")

      text = valid_from_to_text(opening_hours)
      WEEK_DAYS.each do |day_of_week|
        sets = opening_hours.select { |oh| oh["dayOfWeek"].include?(day_of_week) }

        text << opening_hours_text(sets, day_of_week)
      end

      text.compact.join("\n")
    end

    def valid_from_to_text(opening_hours)
      # all should have same from. to values
      set = opening_hours.first
      return [] if set["validFrom"].blank? || set["validThrough"].blank?

      start_date = Time.parse(set["validFrom"])
      end_date   = Time.parse(set["validThrough"])

      [
        I18n.t(
          "valid_from_to",
          start_date: start_date.strftime("%d.%m.%Y"),
          end_date: end_date.strftime("%d.%m.%Y")
        )
      ]
    end

    def opening_hours_text(opening_hours, day_of_week)
      return "#{I18n.t(day_of_week)}: #{I18n.t('closed')}" if opening_hours.blank?
      return "#{I18n.t(day_of_week)}: #{I18n.t('24_hours_opened')}" if all_day?(opening_hours)

      hours = working_hours(opening_hours)

      return "#{I18n.t(day_of_week)}: #{I18n.t('closed')}" if hours.blank?

      "#{I18n.t(day_of_week)}: #{hours}"
    end

    def all_day?(opening_hours)
      opening_hours.select { |o| o["allDay"] == true }.present?
    end
  end
end
