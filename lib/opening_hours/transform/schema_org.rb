# frozen_string_literal: true

module OpeningHours
  module Transform
    class SchemaOrg
      def initialize(entity)
        @entity = entity
      end
      attr_accessor :entity

      def call
        opening_set = entity.delete("openingHoursSpecificationSet")
        entity["openingHoursSpecification"] = []
        entity["openingHoursSpecificationDescription"] = build_description(entity)

        opening_set.each do |set|
          from = set["validFrom"]
          to   = set["validThrough"]

          openings = set["openingHoursSpecification"].group_by { |o| [o["opens"], o["closes"]] }
          openings.each_key do |key|
            hash = build(from, to, openings[key])
            next if hash.blank?

            entity["openingHoursSpecification"] << hash.with_indifferent_access
          end
        end

        entity
      end

      def build_description(entity)
        opening_description = entity["opening_hours_description_translations"]
        return [] if opening_description.blank?

        opening_description.map do |key, value|
          {
            "@language": key,
            "@value": value
          }.with_indifferent_access
        end
      end

      def build(from, to, opening_hours)
        hash = { dayOfWeek: select_value(opening_hours, "dayOfWeek") }

        %w[opens closes allDay].each do |key|
          value = select_value(opening_hours, key)

          hash[key] = value.first if value.present?
        end

        hash["validFrom"] = from if from.present?
        hash["validThrough"] = to if to.present?

        hash
      end

      def select_value(set, key)
        value = set.map { |obj| obj[key] }
        value.compact!
        value.uniq
      end
    end
  end
end
