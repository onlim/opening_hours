# frozen_string_literal: true

module OpeningHours
  class Status < Base
    def call
      specs = select_opening_hours_set(entity["openingHoursSpecification"])
      return I18n.t("no_opening_available") if specs.blank?

      result = openings(specs)

      opening_status_text(result)
    end

    private

    def opening_status_text(result)
      opened = result[:opened]
      date   = result[:current_openings].present? ? (today + 1) : today

      opened ? open_text(result[:current_openings]) : close_text(result[:next_openings], date)
    end

    def open_text(spec)
      text = I18n.t("entity.open")
      return text if spec.blank? || spec["allDay"]

      closes = spec["closes"]

      close_time = Time.zone.parse("#{Date.today} #{closes}").to_i
      last_hour  = (close_time - 3600..close_time).cover?(time)

      text += if last_hour
                I18n.t("entity.close_soon", hour: closes[0..4])
              else
                I18n.t("entity.closes_at", hour: closes[0..4])
              end
      text
    end

    def close_text(spec, date)
      text = I18n.t("entity.closed")
      return text if spec.blank?

      day = next_day(spec["dayOfWeek"], date)
      text += I18n.t("entity.opens_at", hour: spec["opens"][0..4], day: I18n.t(day))
      text
    end

    def openings(hours)
      ranges = select_range(hours, today)

      opened, current_spec = current_spec(ranges)
      next_spec = if current_spec.present?
                    index = ranges.index { |obj| obj[:spec] == current_spec }
                    next_index = index + 1

                    if ranges[next_index]
                      ranges[next_index][:spec]
                    else
                      next_ranges = select_range(hours, today + 1)
                      next_ranges.first[:spec]
                    end
                  else
                    today_next_spec(ranges).presence || current_spec.try(:last) || {}
                  end

      {
        opened: opened,
        current_openings: current_spec,
        next_openings: next_spec
      }
    end

    def current_spec(ranges)
      arr = ranges.find do |obj|
        obj[:spec].try(:[], "allDay") || obj[:range].cover?(time)
      end || {}

      if arr.present?
        opened = true
      else
        arr = ranges.reverse.find { |obj| time > obj[:range].last } || {}
      end

      [opened || false, arr[:spec]]
    end

    def today_next_spec(ranges)
      arr = ranges.find { |obj| time < obj[:range].last }
      return {} if arr.blank?

      arr[:spec]
    end

    def select_range(hours, date)
      wday  = date.strftime("%A")
      specs = hours.select { |oh| oh["dayOfWeek"].include?(wday) }
      return select_range(hours, date + 1) if specs.blank? && wday != today_wday

      ranges = specs.map do |day|
        opens  = Time.zone.parse("#{date} #{day['opens']}").to_i
        closes = Time.zone.parse("#{date} #{day['closes']}").to_i

        { range: opens..closes, spec: day }
      end

      ranges.sort_by { |o| o[:range].first }
    end

    def next_day(spec_wdays, date)
      day = date.strftime("%A")
      return day if spec_wdays.include?(day)

      next_day(spec_wdays, date + 1)
    end

    def today
      @today ||= Time.zone.at(time).to_date
    end

    def today_wday
      today.strftime("%A")
    end
  end
end
