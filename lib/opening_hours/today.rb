# frozen_string_literal: true

module OpeningHours
  class Today < Base
    def call
      specs = select_opening_hours_set(entity["openingHoursSpecification"]) || {}

      opening_hours = specs.select { |oh| oh["dayOfWeek"].include?(Date.today.strftime("%A")) }
      return I18n.t("no_opening_available") if opening_hours.blank?

      working_hours(opening_hours)
    end
  end
end
