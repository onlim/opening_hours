# frozen_string_literal: true

require "active_support/core_ext/hash"
require "active_support/core_ext/time"

require "opening_hours/version"
require "opening_hours/logging"
require "opening_hours/base"
require "opening_hours/list"
require "opening_hours/status"
require "opening_hours/today"

I18n.load_path += Dir.glob("#{File.dirname(__FILE__)}/locales/*.yml")
Time.zone = "Vienna"
module OpeningHours
  class << self
    # 'openingHoursSpecificationSet' => [
    #   {
    #     'name' => 'Whatever'
    #     'validFrom' => date,
    #     'validThrough' => date,
    #     'openingHoursSpecification' => [
    #       {
    #         'dayOfWeek' => 'Monday',
    #         'opens' => '17:00',
    #         'closes' => '22:00',
    #         'allDay' => false
    #       }
    #     ]
    #   }
    # ],

    def list(entity)
      Logging.logger.debug("OpeningHours.list input call: #{entity}")

      List.new(entity).call
    end

    def status(entity)
      Logging.logger.debug("OpeningHours.status input call: #{entity}")

      Status.new(entity).call
    end

    def today(entity)
      Logging.logger.debug("OpeningHours.today input call: #{entity}")

      Today.new(entity).call
    end
  end
end
